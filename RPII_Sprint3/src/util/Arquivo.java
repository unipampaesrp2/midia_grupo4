package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Classe utilit�ria para escrever e ler arquivos de texto
 * 
 * @author Adriel Petry, Allan Pedroso, Eric Oliveira
 *
 */
public class Arquivo {
	private static final String linhaWin = "\r\n";
	private static final String linhaLin = "\n";
	private static String linha;

	/**
	 * L� arquivo de texto.
	 * 
	 * @param arquivo
	 *            Caminho do arquivo de texto
	 * @return String com o conte�do do arquivo de texto
	 * @throws IOException
	 */
	public static String ler(String arquivo) throws IOException {
		if (System.getProperty("os.name").contains("Windows")) {
			linha = linhaWin;
		} else if (System.getProperty("os.name").contains("Linux")) {
			linha = linhaLin;
		}
		BufferedReader leitor = new BufferedReader(new FileReader(new File(
				arquivo)));
		String dado = leitor.readLine();
		String dados = "";
		while (dado != null) {
			dados += dado + linha;
			dado = leitor.readLine();
		}
		leitor.close();
		return dados;
	}

	/**
	 * Escreve em arquivo de texto
	 * 
	 * @param arquivo
	 *            Caminho do arquivo de texto
	 * @param dados
	 *            String com o conte�do a ser escrito no arquivo
	 * @throws IOException
	 */
	public static void escrever(String arquivo, String dados)
			throws IOException {
		BufferedWriter escritor = new BufferedWriter(new FileWriter(new File(
				arquivo)));
		escritor.write(dados);
		escritor.close();
	}

	/**
	 * Escreve dados em um arquivo bin�rio.
	 * 
	 * @param nomeArquivo
	 *            Nome do arquivo a ser escrito
	 * @param objeto
	 *            Objeto a ser escrito no arquivo
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void escreverBin(String nomeArquivo, Object objeto)
			throws FileNotFoundException, IOException {
		ObjectOutputStream output;
		output = new ObjectOutputStream(new FileOutputStream(new File(
				nomeArquivo)));
		output.writeObject(objeto);
		output.close();

	}

	/**
	 * L� um arquivo bin�rio.
	 * 
	 * @param nomeArquivo
	 *            Nome do arquivo a ser lido
	 * @return Dados (Object) do arquivo lido
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object lerBin(String nomeArquivo)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream input;
		input = new ObjectInputStream(
				new FileInputStream(new File(nomeArquivo)));
		Object objeto = input.readObject();
		input.close();
		return objeto;
	}
}
