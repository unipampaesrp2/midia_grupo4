package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe utilit�ria para manipula��o de datas do pacote java.util.Date
 * 
 * @author Allan Pedroso, Eric Oliveira
 *
 */
public class Data {

	private static final SimpleDateFormat fAno = new SimpleDateFormat("yyyy");
	private static final SimpleDateFormat fData = new SimpleDateFormat(
			"dd/MM/yyyy");
	private static final SimpleDateFormat fHora = new SimpleDateFormat("HH:mm");

	/**
	 * Retorna ano (ano) do tipo Date
	 * 
	 * @param data
	 *            Ano no formato "aaaa"
	 * @return Ano (Date) no formato "aaaa"
	 */
	public static Date getAno(int data) {
		String str = String.valueOf(data);
		try {
			Date ano = fAno.parse(str);
			return ano;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Retorna data completa (dia, m�s e ano) do tipo Date
	 * 
	 * @param diaMesAno
	 *            Data completa no formato "dd/mm/aaaa"
	 * @return Data completa (Date) no formato "dd/mm/aaaa"
	 */
	public static Date getData(String diaMesAno) {
		try {
			Date data = fData.parse(diaMesAno);
			return data;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Retorna hora (horas e minutos) do tipo Date
	 * 
	 * @param horas
	 *            Hora no formato "hh:mm"
	 * @return Hora no formato "hh:mm"
	 */
	public static Date getHora(String horas) {
		try {
			Date hora = fHora.parse(horas);
			return hora;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Retorna ano em String no formato "aaaa"
	 * 
	 * @param ano
	 *            Ano
	 * @return Ano no formato "aaaa"
	 */
	public static String anoToString(Date ano) {
		return fAno.format(ano);
	}

	/**
	 * Retorna data completa em String no formato "dd/mm/aaaa"
	 * 
	 * @param data
	 *            Data completa
	 * @return Data completa em String no formato "dd/mm/aaaa"
	 */
	public static String dataToString(Date data) {
		return fData.format(data);
	}

	/**
	 * Retorna hora em String no formato "hh:mm"
	 * 
	 * @param hora
	 *            Hora
	 * @return Hora em String no formato "hh:mm"
	 */
	public static String horaToString(Date hora) {
		return fHora.format(hora);
	}
}
