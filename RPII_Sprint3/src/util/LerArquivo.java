package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LerArquivo {

	private File txt;
	private FileReader arquivo;
	private BufferedReader leitor;

	public boolean setArquivo(String arquivo) {
		txt = new File(arquivo);
		if (txt.exists()) {
			try {
				this.arquivo = new FileReader(txt);
			} catch (FileNotFoundException e) {
				return false;
			}
			leitor = new BufferedReader(this.arquivo);

			return true;
		}
		return false;
	}

	public String getLinha() {
		String linha = "";
		try {
			linha = leitor.readLine();
		} catch (IOException e) {
			return null;
		}
		if (linha != null)
			return linha;
		return null;
	}

	public void fechaArquivo() {
		try {
			leitor.close();
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

}
