package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class EscreverArquivo {
	private File txt;
	private FileWriter arquivo;
	private BufferedWriter escritor;

	public EscreverArquivo(String arquivo) {
		escreveNoInicio(arquivo);
	}

	public EscreverArquivo(String arquivo, boolean fim) {
		if (fim) {
			escreveNoFim(arquivo);
		} else {
			escreveNoInicio(arquivo);
		}
	}

	private void escreveNoInicio(String arquivo) {
		txt = new File(arquivo);
		if (!txt.exists())
			try {
				txt.createNewFile();

			} catch (IOException e) {
				// e.printStackTrace();
			}
		try {
			this.arquivo = new FileWriter(txt);
		} catch (IOException e) {
			// e.printStackTrace();
		}
		escritor = new BufferedWriter(this.arquivo);
	}

	private void escreveNoFim(String arquivo) {
		txt = new File(arquivo);
		if (!txt.exists())
			try {
				txt.createNewFile();
			} catch (IOException e) {
				// e.printStackTrace();
			}
		try {
			this.arquivo = new FileWriter(txt, true);
		} catch (IOException e) {
			// e.printStackTrace();
		}
		escritor = new BufferedWriter(this.arquivo);
	}

	public void setLinha(String linha) {
		try {
			if (linha != null) {
				escritor.write(linha);
				escritor.newLine();
			}
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

	public void fechaArquivo() {
		try {
			escritor.close();
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

}
