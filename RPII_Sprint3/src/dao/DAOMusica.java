package dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeSet;

import util.Arquivo;
import util.Data;
import midias.Midia;
import midias.Musica;

/**
 * Classe DAOMusica que herda atributos e m�todos da classe DAOMidia
 * 
 * @author Eric Oliveira
 *
 */
public class DAOMusica implements IMidia {
	String linha = "\n";
	TreeSet<Musica> musicas = new TreeSet<>();

	/**
	 * Construtor da classe DAOMusica.
	 */
	public DAOMusica() {
		// popularArrayList();
		lerBin();
	}

	/**
	 * @see IMidia#popularArrayList()
	 */
	@Override
	public void popularArrayList() {
		String txt = "";
		try {
			txt = Arquivo.ler("Musicas.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		String dados[] = txt.split(linha);
		for (int i = 1; i < dados.length - 1; i += 10) {
			String nomeDoArquivo = dados[i + 1];
			String titulo = dados[i + 2];
			String descricao = dados[i + 3];
			String genero = dados[i + 4];
			String idioma = dados[i + 5];
			String tempAutores = dados[i + 6];
			String[] autor = tempAutores.split("; ");
			ArrayList<String> autores = new ArrayList<>();
			for (int j = 0; j < autor.length; j++) {
				autores.add(autor[j]);
			}
			Date ano = Data.getAno(Integer.parseInt(dados[i + 7].trim()));
			String tempInterpretes = dados[i + 8];
			String[] interprete = tempInterpretes.split("; ");
			ArrayList<String> interpretes = new ArrayList<>();
			for (int k = 0; k < interprete.length; k++) {
				interpretes.add(interprete[k]);
			}
			int duracao = Integer.parseInt(dados[i + 9].trim());
			Musica musica = new Musica(nomeDoArquivo, titulo, descricao,
					genero, idioma, autores, ano, interpretes, duracao);
			musicas.add(musica);
		}
	}

	/**
	 * L� dados de arquivo bin�rio e popula a lista de m�sicas.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void lerBin() {
		try {
			musicas = (TreeSet<Musica>) Arquivo.lerBin("Musicas");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see IMidia#gravarArquivo()
	 */
	@Override
	public void gravarArquivo() {
		try {
			Arquivo.escreverBin("Musicas", musicas);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// ordenar();
		String dados = String.valueOf(musicas.size()) + linha + linha;
		for (Musica musica : musicas) {
			dados += musica.getNomeDoArquivo() + linha;
			dados += musica.getTitulo() + linha;
			String tempDescricao = musica.getDescricao();
			if (tempDescricao.trim().equalsIgnoreCase("")) {
				dados += "(sem descri��o)" + linha;
			} else {
				dados += tempDescricao + linha;
			}
			dados += musica.getGenero() + linha;
			dados += musica.getIdioma() + linha;
			ArrayList<String> tempAutores = musica.getAutores();
			String temp = "";
			for (int j = 0; j < tempAutores.size(); j++) {
				if (j < tempAutores.size() - 1) {
					temp += tempAutores.get(j) + "; ";
				} else {
					temp += tempAutores.get(j);
				}
			}
			dados += temp + linha;
			dados += Data.anoToString(musica.getAno()) + linha;
			ArrayList<String> tempInterpretes = musica.getInterpretes();
			String temp2 = "";
			for (int j = 0; j < tempInterpretes.size(); j++) {
				if (j < tempInterpretes.size() - 1) {
					temp2 += tempInterpretes.get(j) + "; ";
				} else {
					temp2 += tempInterpretes.get(j);
				}
			}
			dados += temp2 + linha;
			dados += String.valueOf(musica.getDuracao()) + linha;
			dados += linha;
		}
		try {
			Arquivo.escrever("Musicas.txt", dados);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	/**
	 * @see IMidia#ordenar()
	 */
	@Override
	public void ordenar() {
		// // Cocktail Sort
		// Collator compara = Collator.getInstance(new Locale("pt", "BR"));
		// compara.setStrength(Collator.PRIMARY);
		// int tamanho, inicio, fim;
		// boolean troca;
		// tamanho = musicas.size();
		// inicio = 0;
		// fim = tamanho - 1;
		// troca = true;
		// while (troca && inicio < fim) {
		// troca = false;
		// for (int i = inicio; i < fim; i++) {
		// if (compara.compare(musicas.get(i).getTitulo(),
		// musicas.get(i + 1).getTitulo()) > 0) {
		// troca(i, i + 1);
		// troca = true;
		// }
		// }
		// fim--;
		// for (int i = fim; i > inicio; i--) {
		// if (compara.compare(musicas.get(i).getTitulo(),
		// musicas.get(i - 1).getTitulo()) < 0) {
		// troca(i, i - 1);
		// troca = true;
		// }
		// }
		// inicio++;
		// }
		// ordenado = true;
	}

	/**
	 * @see IMidia#add(Midia)
	 */
	@Override
	public void add(Midia midia) {
		musicas.add((Musica) midia);
	}
	
	/**
	 * @see IMidia#get(String)
	 */
	@Override
	public Midia get(String titulo) {
		for (Musica musica : musicas) {
			if (musica.getTitulo().equalsIgnoreCase(titulo)) {
				return (Midia) musica;
			}
		}
		// return musicas.get(buscaBinaria(titulo));
		return null;
	}
	
	/**
	 * @see IMidia#altera(String, Midia)
	 */
	@Override
	public void altera(String titulo, Midia midia) {
		Musica aux = (Musica) midia;
		Musica array[] = musicas.toArray(new Musica[musicas.size()]);
		for (Musica musica : array) {
			if (musica.getTitulo().equalsIgnoreCase(titulo)) {
				musicas.remove(musica);
				musicas.add(aux);
			}
		}
		// musicas.set(buscaBinaria(titulo), (Musica) midia);
	}
	
	/**
	 * @see IMidia#exclui(String)
	 */
	@Override
	public void exclui(String titulo) {
		Musica array[] = musicas.toArray(new Musica[musicas.size()]);
		for (Musica musica : array) {
			if (musica.getTitulo().equalsIgnoreCase(titulo)) {
				musicas.remove(musica);
			}
		}
	}
	
	/**
	 * @see IMidia#existe(String)
	 */
	@Override
	public boolean existe(String titulo) {
		String titulos[] = getTitulos();
		for (int i = 0; i < titulos.length; i++) {
			if (titulo.equalsIgnoreCase(titulos[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica se uma m�sica (com o caminho absoluto da mesma) j� foi
	 * adicionada � lista de m�sicas.
	 * 
	 * @param nomeDoArquivo
	 *            Caminho absoluto da m�sica.
	 * @return True, se a m�sica j� est� adiciona, e false, caso contr�rio.
	 */
	public boolean jaAdicionada(String nomeDoArquivo) {
		Musica array[] = musicas.toArray(new Musica[musicas.size()]);
		for (int i = 0; i < array.length; i++) {
			if (array[i].getNomeDoArquivo().contains(nomeDoArquivo)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Agrupa as m�sicas por g�nero, idioma ou ano.
	 * 
	 * @param tipoDeAgrupamento
	 *            Tipo de agrupamento
	 * @param valor
	 *            Valor do agrupamento
	 * @return String com as m�sicas agrupadas
	 */
	public String agrupaPor(String tipoDeAgrupamento, String valor) {
		String retorno = "";
		if (tipoDeAgrupamento.equalsIgnoreCase("Genero")) {
			retorno += agrupaPorGenero(valor);
		} else if (tipoDeAgrupamento.equalsIgnoreCase("Idioma")) {
			retorno += agrupaPorIdioma(valor);
		} else if (tipoDeAgrupamento.equalsIgnoreCase("Ano")) {
			retorno += agrupaPorAno(valor);
		}
		return retorno;
	}

	/**
	 * Agrupa as m�sicas por g�nero.
	 * 
	 * @param genero
	 *            G�nero escolhido
	 * @return String com as m�sicas agrupadas por g�nero
	 */
	private String agrupaPorGenero(String genero) {
		String retorno = "";
		for (Musica musica : musicas) {
			if (musica.getGenero().equalsIgnoreCase(genero)) {
				retorno += musica.toString() + "\n";
			}
		}
		return retorno;
	}

	/**
	 * Agrupa as m�sicas por idioma.
	 * 
	 * @param idioma
	 *            Idioma escolhido
	 * @return String com as m�sicas agrupadas por idioma
	 */
	private String agrupaPorIdioma(String idioma) {
		String retorno = "";
		for (Musica musica : musicas) {
			if (musica.getIdioma().equalsIgnoreCase(idioma)) {
				retorno += musica.toString() + "\n";
			}
		}
		return retorno;
	}

	/**
	 * Agrupa as m�sicas por ano.
	 * 
	 * @param ano
	 *            Ano escolhido
	 * @return String com as m�sicas agrupadas por ano
	 */
	private String agrupaPorAno(String ano) {
		String retorno = "";
		for (Musica musica : musicas) {
			if (Data.anoToString(musica.getAno()).equalsIgnoreCase(ano)) {
				retorno += musica.toString() + "\n";
			}
		}
		return retorno;
	}

	/**
	 * Retorna os t�tulos das m�sicas cadastradas.
	 * 
	 * @return Array de String com os t�tulos das m�sicas cadastradas
	 */
	public String[] getTitulos() {
		TreeSet<String> titulos = new TreeSet<>();
		for (Musica musica : musicas) {
			titulos.add(musica.getTitulo());
		}
		return titulos.toArray(new String[titulos.size()]);
	}

	/**
	 * Retorna os g�neros das m�sicas cadastradas.
	 * 
	 * @return Array de String com os g�neros das m�sicas cadastradas
	 */
	public String[] getGeneros() {
		TreeSet<String> generos = new TreeSet<>();
		for (Musica musica : musicas) {
			generos.add(musica.getGenero());
		}
		return generos.toArray(new String[generos.size()]);
	}

	/**
	 * Retorna os idiomas das m�sicas cadastradas.
	 * 
	 * @return Array de String com os idiomas das m�sicas cadastradas
	 */
	public String[] getIdiomas() {
		TreeSet<String> idiomas = new TreeSet<>();
		for (Musica musica : musicas) {
			idiomas.add(musica.getIdioma());
		}
		return idiomas.toArray(new String[idiomas.size()]);
	}

	/**
	 * Retorna os anos das m�sicas cadastradas.
	 * 
	 * @return Array de String com os anos das m�sicas cadastradas
	 */
	public String[] getAnos() {
		TreeSet<String> anos = new TreeSet<>();
		for (Musica musica : musicas) {
			anos.add(Data.anoToString(musica.getAno()));
		}
		return anos.toArray(new String[anos.size()]);
	}

	/**
	 * @see IMidia#toString()
	 */
	@Override
	public String toString() {
		if (musicas.size() > 0) {
			String completo = "";
			for (Musica musica : musicas) {
				completo += musica.toString() + "\n";
			}
			return completo;
		}
		return "N�o h� m�sicas cadastradas.";
	}

	/**
	 * @see IMidia#toString(String titulo)
	 */
	@Override
	public String toString(String titulo) {
		if (existe(titulo)) {
			return ((Musica) get(titulo)).toString();
		}
		return null;
	}

}
