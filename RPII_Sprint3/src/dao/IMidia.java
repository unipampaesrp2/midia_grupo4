package dao;

import midias.Midia;

/**
 * Classe abstrata contendo todos os m�todos de manipula��o do objeto midia.
 * 
 * @author Adriel Petry, Allan Pedroso, Eric Oliveira
 *
 */

public interface IMidia {

	/**
	 * M�todo abstrato que recebe um arquivo por parametro o l� e popula a
	 * cole��o.
	 */
	public abstract void popularArrayList();

	/**
	 * M�todo que grava um arquivo bin�rio e um txt com todos os objetos do tipo midia
	 * gravados na cole��o.
	 */
	public abstract void gravarArquivo();

	/**
	 * M�todo abstrato para ordena��o das midias
	 */
	public abstract void ordenar();

	/**
	 * M�todo de inclus�o de midias.
	 * 
	 * @param midia
	 */
	public abstract void add(Midia midia);

	/**
	 * M�todo que retorna m�dia com determinado t�tulo
	 * 
	 * @param titulo
	 *            T�tulo da m�dia
	 * @return M�dia
	 */
	public abstract Midia get(String titulo);

	/**
	 * M�todo para altera��o do objeto
	 * 
	 * @param titulo
	 *            titulo da midia
	 * @param midia
	 *            Objeto midia
	 */
	public abstract void altera(String titulo, Midia midia);

	/**
	 * M�todo que exclui m�dia com determinado t�tulo
	 * 
	 * @param titulo
	 *            T�tulo da m�dia
	 */
	public void exclui(String titulo);

	/**
	 * M�todo que verifica a exist�ncia de uma m�dia na cole��o.
	 * 
	 * @param titulo
	 *            titulo da midia.
	 * @return false, se a m�dia existe, ou true, caso contr�rio.
	 */
	public abstract boolean existe(String titulo);

	/**
	 * M�todo abstrato para exibi��o dos dados da m�dia.
	 */
	public abstract String toString();

	/**
	 * M�todo abstrato para exibi��o dos dados da m�dia recebendo por
	 * parametro o titulo da midia.
	 */
	public abstract String toString(String titulo);
}
