package dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TreeSet;

import util.Arquivo;
import util.Data;
import util.EscreverArquivo;
import util.LerArquivo;
import midias.Midia;
import midias.Musica;
import midias.Podcast;

/**
 * Classe para manipulação de objetos
 * 
 * @author Allan Pedroso
 *
 */
public class DAOPodcast implements IMidia {
	String linha = "\n";
	
	ArrayList<Podcast> podcasts = new ArrayList<Podcast>();

	public DAOPodcast() {

		popularArrayList();
	}

	/**
	 * Método que recebe um arquivo por parametro o lê e popula o arrayList.
	 */
	@Override
	public void popularArrayList() {

		try {
			podcasts = (ArrayList<Podcast>) Arquivo.lerBin("Podcasts");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
		}

	}

	/**
	 * Método que grava um arquivo txt com todos objetos armazenados no array.
	 */
	@Override
	public void gravarArquivo() {
		ordenar();
		try {
			Arquivo.escreverBin("Podcasts", podcasts);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String dados = String.valueOf(podcasts.size()) + linha + linha;
		for (Podcast podcast : podcasts) {
			dados += podcast.getNomeDoArquivo() + linha;
			dados += podcast.getTitulo() + linha;
			String tempDescricao = podcast.getDescricao();
			if (tempDescricao.trim().equalsIgnoreCase("")) {
				dados += "(sem descrição)" + linha;
			} else {
				dados += tempDescricao + linha;
			}
			
			dados += podcast.getIdioma() + linha;
			ArrayList<String> tempAutores = podcast.getAutores();
			String temp = "";
			for (int j = 0; j < tempAutores.size(); j++) {
				if (j < tempAutores.size() - 1) {
					temp += tempAutores.get(j) + "; ";
				} else {
					temp += tempAutores.get(j);
				}
			}
			dados += temp + linha;
			dados += Data.anoToString(podcast.getAno()) + linha;
			dados += linha;
		}
		try {
			Arquivo.escrever("Podcasts.txt", dados);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		
		
		
	}

	public boolean existe(String titulo) {
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getTitulo().equalsIgnoreCase(titulo)) {
				return true;
			}
		}
		return false;
	}

	
	public boolean existeArquivo(String nomeDoArquivo) {
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getNomeDoArquivo().equalsIgnoreCase(nomeDoArquivo)) {
				return true;
			}
		}
		return false;
	}
	
	
	
	
	public void altera(String titulo, Midia midia) {
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getTitulo().equalsIgnoreCase(titulo)) {
				podcasts.set(i, (Podcast) midia);
			}
		}
	}

	/**
	 * Método que ordena os podcasts em ordem alfabética pelo titulo do podcast
	 * .
	 */
	@Override
	public void ordenar() {

		// BubbleSort
		Collator compara = Collator.getInstance(new Locale("pt", "BR"));
		compara.setStrength(Collator.PRIMARY);
		for (int i = 0; i < podcasts.size() - 1; ++i) {
			for (int j = i + 1; j < podcasts.size(); ++j) {
				if (compara.compare(podcasts.get(i).getTitulo(), podcasts
						.get(j).getTitulo()) > 0) {

					Podcast temp = podcasts.get(i);
					podcasts.set(i, podcasts.get(j));
					podcasts.set(j, temp);
				}
			}
		}
	}

	public Podcast get(String titulo) {
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getTitulo().equalsIgnoreCase(titulo)) {
				return podcasts.get(i);
			}
		}
		return null;
	}

	public void exclui(String titulo) {
		for (int i = 0; i < podcasts.size(); i++) {
			if (podcasts.get(i).getTitulo().equalsIgnoreCase(titulo)) {
				podcasts.remove(i);
			}
		}

	}

	/**
	 * Método que retorna uma string com todos os podcasts cadastrados e todos
	 * os seus dados.
	 */
	@Override
	public String toString() {
		ordenar();
		String dadosPodcast = "";
		if (podcasts.size() > 0) {
			for (int i = 0; i < podcasts.size(); i++) {
				dadosPodcast += podcasts.get(i).toString() + "\n" + "\n";
			}

			return dadosPodcast;
		}
		return null;
	}

	public int buscaBinaria(String titulo) {
		Collator compara = Collator.getInstance(new Locale("pt", "BR"));
		int e, p, d;
		e = 0;
		d = podcasts.size() - 1;
		while (e <= d) {
			p = (e + d) / 2;
			if (compara.compare(podcasts.get(p).getTitulo(), titulo) == 0)
				return p;
			if (compara.compare(podcasts.get(p).getTitulo(), titulo) < 0)
				e = p + 1;
			else
				d = p - 1;
		}
		return -1;
	}

	/**
	 * Método que retorna uma string com todos os dados de um determinado
	 * podcast.
	 */
	@Override
	public String toString(String titulo) {
		ordenar();
		for (int i = 0; i < podcasts.size(); i++) {

			if (existe(titulo)) {
				return get(titulo).toString();

			}

		}
		return null;
	}

	public String agrupar(String tipoAgrupamento, String valor) {
		String agrupaPor = "";
		if (tipoAgrupamento.equalsIgnoreCase("Idioma")) {
			agrupaPor += agrupaPorIdioma(valor);
		} else if (tipoAgrupamento.equalsIgnoreCase("Ano")) {
			agrupaPor += agrupaPorAno(valor);
		}
		return agrupaPor;
	}

	private String agrupaPorIdioma(String valor) {
		String retorno = "";
		for (Podcast podcast : podcasts) {
			if (podcast.getIdioma().equalsIgnoreCase(valor)) {
				retorno += podcast.toString() + "\n";
			}
		}
		return retorno;
	}

	private String agrupaPorAno(String valor) {
		String retorno = "";
		for (Podcast podcast : podcasts) {
			if (Data.anoToString(podcast.getAno()).equalsIgnoreCase(valor)) {
				retorno += podcast.toString() + "\n";
			}
		}
		return retorno;
	}
	
	
	public String[] getTitulos() {
		ArrayList<String> titulos = new ArrayList<>();
		for (Podcast podcast : podcasts) {
			titulos.add(podcast.getTitulo());
		}
		return titulos.toArray(new String[titulos.size()]);
	}
	

	public String[] getIdiomas() {
		TreeSet<String> idiomas = new TreeSet<>();
		for (Podcast podcast : podcasts) {
			idiomas.add(podcast.getIdioma());
		}
		return idiomas.toArray(new String[idiomas.size()]);
	}

	public String[] getAnos() {
		TreeSet<String> anos = new TreeSet<>();
		for (Podcast podcast : podcasts) {
			anos.add(Data.anoToString(podcast.getAno()));
		}
		return anos.toArray(new String[anos.size()]);
	}

	@Override
	public void add(Midia podcast) {
		podcasts.add((Podcast) podcast);

	}
}