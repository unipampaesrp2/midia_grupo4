package dao;

import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import midias.Midia;
import midias.Foto;
import util.Arquivo;
import util.Data;

/**
 * Classe DAOFoto que herda atributos e m�todos da classe DAOMidia
 * 
 * @author Matheus Aguiar, Eric Oliveira
 *
 */
public class DAOFoto extends IMidia {

	/**
	 * Construtor da classe DAOFoto
	 * 
	 */
	public DAOFoto() {
		popularArrayList();
	}

	/**
	 * @see DAOMidia#popularArrayList()
	 */
	public void popularArrayList() {
		String txt = "";
		try {
			txt = Arquivo.ler("Fotos.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		String dados[] = txt.split(linha);
		for (int i = 1; i < dados.length; i += 9) {
			String nomeDoArquivo = dados[i + 1];
			String titulo = dados[i + 2];
			String descricao = dados[i + 3];
			String local = dados[i + 4];
			String fotografo = dados[i + 5];
			String tempPessoas = dados[i + 6];
			String[] pessoa = tempPessoas.split("; ");
			ArrayList<String> pessoas = new ArrayList<>();
			for (int j = 0; j < pessoa.length; j++) {
				pessoas.add(pessoa[j]);
			}
			Date data = Data.getData(dados[i + 7]);
			Date hora = Data.getHora(dados[i + 8]);
			Midia foto = new Foto(nomeDoArquivo, titulo, descricao, fotografo,
					pessoas, local, data, hora);
			midias.add(foto);
		}
	}

	/**
	 * @see DAOMidia#gravarArquivo()
	 */
	@Override
	public void gravarArquivo() {
		ordenar();
		String dados = String.valueOf(midias.size()) + linha + linha;
		for (int i = 0; i < midias.size(); i++) {
			dados += ((Foto) midias.get(i)).getNomeDoArquivo() + linha;
			dados += ((Foto) midias.get(i)).getTitulo() + linha;
			String tempDescricao = ((Foto) midias.get(i)).getDescricao();
			if (tempDescricao.trim().equalsIgnoreCase("")) {
				dados += "(sem descri��o)" + linha;
			} else {
				dados += tempDescricao + linha;
			}
			dados += ((Foto) midias.get(i)).getLocal() + linha;
			dados += ((Foto) midias.get(i)).getFotografo() + linha;
			ArrayList<String> tempPessoas = ((Foto) midias.get(i)).getPessoas();
			String temp = "";
			for (int j = 0; j < tempPessoas.size(); j++) {
				if (j < tempPessoas.size() - 1) {
					temp += tempPessoas.get(j) + "; ";
				} else {
					temp += tempPessoas.get(j);
				}
			}
			dados += temp + linha;
			dados += Data.dataToString(((Foto) midias.get(i)).getData())
					+ linha;
			dados += Data.horaToString(((Foto) midias.get(i)).getHora())
					+ linha;
			dados += linha;
		}
		try {
			Arquivo.escrever("Fotos.txt", dados);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	/**
	 * @see DAOMidia#ordenar()
	 */
	@Override
	public void ordenar() {
		// Cocktail Sort
		Collator compara = Collator.getInstance(new Locale("pt", "BR"));
		compara.setStrength(Collator.PRIMARY);
		int tamanho, inicio, fim;
		boolean troca;
		tamanho = midias.size();
		inicio = 0;
		fim = tamanho - 1;
		troca = true;
		while (troca && inicio < fim) {
			troca = false;
			for (int i = inicio; i < fim; i++) {
				if (compara.compare(midias.get(i).getTitulo(), midias
						.get(i + 1).getTitulo()) > 0) {
					troca(i, i + 1);
					troca = true;
				}
			}
			fim--;
			for (int i = fim; i > inicio; i--) {
				if (compara.compare(midias.get(i).getTitulo(), midias
						.get(i - 1).getTitulo()) < 0) {
					troca(i, i - 1);
					troca = true;
				}
			}
			inicio++;
		}
	}

	/**
	 * Troca duas m�dias de posi��o
	 * 
	 * @param i
	 *            �ndice da primeira m�dia
	 * @param j
	 *            �ndice da segunda m�dia
	 */
	private void troca(int i, int j) {
		Midia aux = midias.get(i);
		midias.set(i, midias.get(j));
		midias.set(j, aux);
	}

	/**
	 * @see DAOMidia#toString()
	 */
	@Override
	public String toString() {
		ordenar();
		String str = "";
		for (int i = 0; i < midias.size(); i++) {
			str += ((Foto) midias.get(i)).toString() + "\n";
		}
		return str;
	}

	/**
	 * @see DAOMidia#toString(String titulo)
	 */
	@Override
	public String toString(String titulo) {
		for (int i = 0; i < midias.size(); i++) {
			if (midias.get(i).getTitulo().equalsIgnoreCase(titulo)) {
				return ((Foto) midias.get(i)).toString();
			}
		}
		return null;
	}

}