package usuario;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import midias.Midia;
import midias.Musica;
import util.Data;
import dao.DAOMusica;

/**
 * Classe IMusica
 * 
 * @author Eric Oliveira
 *
 */
public class IMusica {
	private DAOMusica musicas;

	/**
	 * Construtor da classe IMusica
	 * 
	 */
	public IMusica() {
		musicas = new DAOMusica();
	}

	/**
	 * Recebe dados de m�sica e a retorna como objeto Midia
	 * 
	 * @return M�sica
	 */
	public Midia getMusica() {
		JOptionPane.showMessageDialog(null,
				"Escolha a m�sica na pr�xima janela.", "M�sica",
				JOptionPane.PLAIN_MESSAGE);
		String caminho = "";
		String userHome = System.getProperty("user.home");
		JFileChooser nomeArquivo = new JFileChooser(userHome);
		boolean erro = false;
		String nomeDoArquivo = "", titulo = "";
		do {
			try {
				int op = nomeArquivo.showOpenDialog(null);
				if (op == JFileChooser.APPROVE_OPTION) {
					File arquivo = nomeArquivo.getSelectedFile();
					caminho = arquivo.toString();
				}
				nomeDoArquivo = caminho;
				if (musicas.jaAdicionada(nomeDoArquivo)) {
					erro = true;
					throw new IllegalArgumentException(
							"A m�sica j� est� cadastrada.");
				}
				titulo = JOptionPane.showInputDialog(null, "T�tulo", "M�sica",
						JOptionPane.PLAIN_MESSAGE);
				erro = false;
			} catch (IllegalArgumentException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(),
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);

			}
		} while (erro);
		String descricao = JOptionPane.showInputDialog(null,
				"Descri��o\nDeixe em branco se n�o tiver uma", "M�sica",
				JOptionPane.PLAIN_MESSAGE);
		if (descricao.trim().isEmpty() || descricao == null) {
			descricao = "(sem descri��o)";
		}
		String genero = "";
		do {
			try {
				genero = JOptionPane.showInputDialog(null, "G�nero", "M�sica",
						JOptionPane.PLAIN_MESSAGE);
				if (genero.matches(".*\\d+.*")) {
					throw new IllegalArgumentException("Informe apenas letras.");
				}
				erro = false;
			} catch (IllegalArgumentException e) {
				erro = true;
				JOptionPane.showMessageDialog(null, e.getMessage(),
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} while (erro);
		String idioma = "";
		do {
			try {
				idioma = JOptionPane.showInputDialog(null, "Idioma", "M�sica",
						JOptionPane.PLAIN_MESSAGE);
				if (idioma.matches(".*\\d+.*")) {
					throw new IllegalArgumentException("Informe apenas letras.");
				}
				erro = false;
			} catch (IllegalArgumentException e) {
				erro = true;
				JOptionPane.showMessageDialog(null, e.getMessage(),
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} while (erro);
		ArrayList<String> autores = new ArrayList<>();
		int opcao;
		do {
			autores.add(JOptionPane.showInputDialog(null, "Autor", "M�sica",
					JOptionPane.PLAIN_MESSAGE));
			opcao = JOptionPane.showConfirmDialog(null,
					"Deseja adicionar outro autor?", "M�sica",
					JOptionPane.YES_NO_OPTION);
		} while (opcao == JOptionPane.YES_OPTION);
		Date ano = null;
		do {
			try {
				ano = Data.getAno(Integer.parseInt(JOptionPane.showInputDialog(
						null, "Ano", "M�sica", JOptionPane.PLAIN_MESSAGE)));
				erro = false;
			} catch (Exception e) {
				erro = true;
				JOptionPane.showMessageDialog(null, "Informe apenas n�meros",
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} while (erro);
		erro = false;
		ArrayList<String> interpretes = new ArrayList<>();
		do {
			interpretes.add(JOptionPane.showInputDialog(null, "Int�rprete",
					"M�sica", JOptionPane.PLAIN_MESSAGE));
			opcao = JOptionPane.showConfirmDialog(null,
					"Deseja adicionar outro int�rprete?", "M�sica",
					JOptionPane.YES_NO_OPTION);
		} while (opcao == JOptionPane.YES_OPTION);
		int min = 0, seg = 0;
		do {
			try {
				min = Integer.parseInt(JOptionPane
						.showInputDialog(null, "Dura��o: Minutos", "M�sica",
								JOptionPane.PLAIN_MESSAGE));
				if (min < 0 || min > 59) {
					throw new IllegalArgumentException(
							"Informe apenas n�meros inteiros entre 0 e 59.");
				}
				erro = false;
			} catch (NumberFormatException e) {
				erro = true;
				JOptionPane.showMessageDialog(null, "Informe apenas n�meros",
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			} catch (IllegalArgumentException e) {
				erro = true;
				JOptionPane.showMessageDialog(null, e.getMessage(),
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} while (erro);
		do {
			try {
				seg = Integer.parseInt(JOptionPane.showInputDialog(null,
						"Dura��o: Segundos", "M�sica",
						JOptionPane.PLAIN_MESSAGE));
				if (seg < 0 || seg > 59) {
					throw new IllegalArgumentException(
							"Informe apenas n�meros inteiros entre 0 e 59.");
				}
				erro = false;
			} catch (NumberFormatException e) {
				erro = true;
				JOptionPane.showMessageDialog(null, "Informe apenas n�meros",
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			} catch (IllegalArgumentException e) {
				erro = true;
				JOptionPane.showMessageDialog(null, e.getMessage(),
						"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} while (erro);
		erro = false;
		int duracao = min * 60 + seg;
		Midia musica;
		try {
			musica = new Musica(nomeDoArquivo, titulo, descricao, genero,
					idioma, autores, ano, interpretes, duracao);
		} catch (Exception e) {
			return null;
		}
		return musica;
	}

	/**
	 * Adiciona m�sicas ao banco de m�sicas
	 * 
	 */
	private void add() {
		int nova;
		do {
			Midia musica = getMusica();
			if (musica != null) {
				musicas.add(musica);
				JOptionPane.showMessageDialog(null,
						"M�sica adicionada com sucesso!", "M�sica",
						JOptionPane.PLAIN_MESSAGE);
			} else {
				JOptionPane
						.showMessageDialog(
								null,
								"N�o foi poss�vel adicionar a m�sica.\nDados insuficientes",
								"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
			}
			nova = JOptionPane.showConfirmDialog(null,
					"Deseja adicionar outra m�sica?", "M�sica",
					JOptionPane.YES_NO_OPTION);
		} while (nova == JOptionPane.YES_OPTION);
	}

	/**
	 * Altera dados de uma m�sica
	 * 
	 */
	private void altera() {
		String[] titulos = musicas.getTitulos();
		String titulo = (String) JOptionPane.showInputDialog(null,
				"Selecione a m�sica que deseja alterar.", "M�sica",
				JOptionPane.PLAIN_MESSAGE, null, titulos, titulos[0]);
		if (titulo != null) {
			int alteraOutroCampo = JOptionPane.NO_OPTION;
			Midia musica = musicas.get(titulo);
			String menu[] = { "Descri��o", "G�nero", "Idioma", "Autores",
					"Ano", "Int�rpretes", "Dura��o" };
			do {
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Escolha o campo que deseja alterar",
						"M�sica: Altera��o", JOptionPane.PLAIN_MESSAGE, null,
						menu, menu[0]);
				if (opcao != null) {
					if (opcao.equalsIgnoreCase("Descri��o")) {
						String descricao = JOptionPane.showInputDialog(null,
								"Descri��o", "M�sica",
								JOptionPane.PLAIN_MESSAGE);
						((Musica) musica).setDescricao(descricao);
					} else if (opcao.equalsIgnoreCase("G�nero")) {
						String genero = JOptionPane.showInputDialog(null,
								"G�nero", "M�sica", JOptionPane.PLAIN_MESSAGE);
						((Musica) musica).setGenero(genero);
					} else if (opcao.equalsIgnoreCase("Idioma")) {
						String idioma = JOptionPane.showInputDialog(null,
								"Idioma", "M�sica", JOptionPane.PLAIN_MESSAGE);
						((Musica) musica).setIdioma(idioma);
					} else if (opcao.equalsIgnoreCase("Autores")) {
						ArrayList<String> autores = new ArrayList<>();
						int novoAutor;
						do {
							autores.add(JOptionPane.showInputDialog(null,
									"Autor", "M�sica",
									JOptionPane.PLAIN_MESSAGE));
							novoAutor = JOptionPane.showConfirmDialog(null,
									"Deseja adicionar outro autor?", "M�sica",
									JOptionPane.YES_NO_OPTION);
						} while (novoAutor == JOptionPane.YES_OPTION);
						((Musica) musica).setAutores(autores);
					} else if (opcao.equalsIgnoreCase("Ano")) {
						boolean erro = false;
						Date ano = null;
						do {
							try {
								ano = Data.getAno(Integer.parseInt(JOptionPane
										.showInputDialog(null, "Ano", "M�sica",
												JOptionPane.PLAIN_MESSAGE)));
								erro = false;
							} catch (Exception e) {
								erro = true;
								JOptionPane.showMessageDialog(null,
										"Informe apenas n�meros",
										"M�sica: Erro",
										JOptionPane.ERROR_MESSAGE);
							}
						} while (erro);
						((Musica) musica).setAno(ano);
					} else if (opcao.equalsIgnoreCase("Int�rpretes")) {
						int novoInterprete;
						ArrayList<String> interpretes = new ArrayList<>();
						do {
							interpretes.add(JOptionPane.showInputDialog(null,
									"Int�rprete", "M�sica",
									JOptionPane.PLAIN_MESSAGE));
							novoInterprete = JOptionPane.showConfirmDialog(
									null, "Deseja adicionar outro int�rprete?",
									"M�sica", JOptionPane.YES_NO_OPTION);
						} while (novoInterprete == JOptionPane.YES_OPTION);
						((Musica) musica).setInterpretes(interpretes);
					} else if (opcao.equalsIgnoreCase("Dura��o")) {
						boolean erro = false;
						int min = 0, seg = 0;
						do {
							try {
								min = Integer.parseInt(JOptionPane
										.showInputDialog(null,
												"Dura��o: Minutos", "M�sica",
												JOptionPane.PLAIN_MESSAGE));
								seg = Integer.parseInt(JOptionPane
										.showInputDialog(null,
												"Dura��o: Segundos", "M�sica",
												JOptionPane.PLAIN_MESSAGE));
								erro = false;
							} catch (Exception e) {
								erro = true;
								JOptionPane.showMessageDialog(null,
										"Informe apenas n�meros",
										"M�sica: Erro",
										JOptionPane.ERROR_MESSAGE);
							}
						} while (erro);
						int duracao = min * 60 + seg;
						((Musica) musica).setDuracao(duracao);
					}
					alteraOutroCampo = JOptionPane.showConfirmDialog(null,
							"Deseja alterar outro campo?", "M�sica: Altera��o",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.PLAIN_MESSAGE);
				}
			} while (alteraOutroCampo == JOptionPane.YES_OPTION);
			musicas.altera(titulo, musica);
			JOptionPane.showMessageDialog(null,
					"A m�sica foi alterada com sucesso.", "M�sica",
					JOptionPane.PLAIN_MESSAGE);
		}/*
		 * else { JOptionPane.showMessageDialog(null,
		 * "N�o h� m�sicas cadastradas com esse t�tulo.", "M�sica: Erro",
		 * JOptionPane.ERROR_MESSAGE); }
		 */
	}

	/**
	 * Exibe dados de uma ou de todas as m�sicas
	 * 
	 */
	private void exibe() {
		String menu[] = { "Uma m�sica", "Todas as m�sicas" };
		String opcao = (String) JOptionPane.showInputDialog(null, "Menu",
				"M�sica", JOptionPane.PLAIN_MESSAGE, null, menu, menu[0]);
		if (opcao.equalsIgnoreCase("Uma m�sica")) {
			String[] titulos = musicas.getTitulos();
			String titulo = (String) JOptionPane.showInputDialog(null,
					"Selecione a m�sica que deseja exibir.", "M�sica",
					JOptionPane.PLAIN_MESSAGE, null, titulos, titulos[0]);
			if (titulo != null) {
				JOptionPane.showMessageDialog(null, musicas.get(titulo),
						"M�sica", JOptionPane.PLAIN_MESSAGE);

			}
		} else if (opcao == null || opcao.equalsIgnoreCase("Todas as m�sicas")) {
			JOptionPane.showMessageDialog(null, exibeTodos(), "M�sica",
					JOptionPane.PLAIN_MESSAGE);
		}
	}

	/**
	 * Exclui uma m�sica do banco de m�sicas
	 * 
	 */
	private void exclui() {
		String[] titulos = musicas.getTitulos();
		String titulo = (String) JOptionPane.showInputDialog(null,
				"Selecione a m�sica que deseja excluir.", "M�sica",
				JOptionPane.PLAIN_MESSAGE, null, titulos, titulos[0]);
		if (titulo != null) {
			musicas.exclui(titulo);
			JOptionPane.showMessageDialog(null,
					"A m�sica foi exclu�da com sucesso.", "M�sica",
					JOptionPane.PLAIN_MESSAGE);
		}
	}

	private void pesquisa() {
		String titulo = JOptionPane.showInputDialog(null,
				"Informe o t�tulo da m�sica que deseja pesquisar.", "M�sica",
				JOptionPane.PLAIN_MESSAGE);
		if (musicas.existe(titulo)) {
			JOptionPane.showMessageDialog(null, musicas.get(titulo).toString(),
					"M�sica", JOptionPane.PLAIN_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null,
					"N�o h� m�sicas cadastradas com esse t�tulo.",
					"M�sica: Erro", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Agrupa m�sicas por g�nero, idioma ou ano.
	 * 
	 */
	private void agrupa() {
		JButton genero = new JButton();
		genero.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaGenero.png")));
		genero.setBackground(new Color(210, 210, 210));
		genero.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String[] generos = musicas.getGeneros();
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Qual o g�nero a ser exibido?", "M�sica",
						JOptionPane.PLAIN_MESSAGE, null, generos, generos[0]);
				if (opcao != null) {
					JOptionPane.showMessageDialog(null,
							exibeAgrupado(musicas.agrupaPor("Genero", opcao)),
							"M�sica - G�nero: " + opcao,
							JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

		JButton idioma = new JButton();
		idioma.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaIdioma.png")));
		idioma.setBackground(new Color(210, 210, 210));
		idioma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String[] idiomas = musicas.getIdiomas();
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Qual o idioma a ser exibido?", "M�sica",
						JOptionPane.PLAIN_MESSAGE, null, idiomas, idiomas[0]);
				if (opcao != null) {
					JOptionPane.showMessageDialog(null,
							exibeAgrupado(musicas.agrupaPor("Idioma", opcao)),
							"M�sica - Idioma: " + opcao,
							JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

		JButton ano = new JButton();
		ano.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaAno.png")));
		ano.setBackground(new Color(210, 210, 210));
		ano.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String[] anos = musicas.getAnos();
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Qual o ano a ser exibido?", "M�sica",
						JOptionPane.PLAIN_MESSAGE, null, anos, anos[0]);
				if (opcao != null) {
					JOptionPane.showMessageDialog(null,
							exibeAgrupado(musicas.agrupaPor("Ano", opcao)),
							"M�sica - Ano: " + opcao, JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

		Object[] menu = { genero, idioma, ano };
		JOptionPane.showMessageDialog(null, menu, "Menu: M�sica",
				JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Menu principal da classe
	 * 
	 */
	public void menu() {
		JLabel lblMusica = new JLabel("");
		lblMusica.setHorizontalAlignment(SwingConstants.CENTER);
		lblMusica.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/lblMusica.png")));

		JButton adiciona = new JButton();
		adiciona.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaAdicionar.png")));
		adiciona.setBackground(new Color(210, 210, 210));
		adiciona.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				add();

			}
		});

		JButton altera = new JButton();
		altera.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaAlterar.png")));
		altera.setBackground(new Color(210, 210, 210));
		altera.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				altera();

			}
		});

		JButton exibe = new JButton();
		exibe.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaExibir.png")));
		exibe.setBackground(new Color(210, 210, 210));
		exibe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exibe();

			}
		});

		JButton pesquisa = new JButton();
		pesquisa.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaPesquisar.png")));
		pesquisa.setBackground(new Color(210, 210, 210));
		pesquisa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pesquisa();

			}
		});

		JButton agrupa = new JButton();
		agrupa.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaAgrupar.png")));
		agrupa.setBackground(new Color(210, 210, 210));
		agrupa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agrupa();

			}
		});

		JButton exclui = new JButton();
		exclui.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/musicaExcluir.png")));
		exclui.setBackground(new Color(210, 210, 210));
		exclui.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exclui();

			}
		});

		Object[] menu = { lblMusica, adiciona, altera, exibe,/* pesquisa, */
				agrupa, exclui };
		JOptionPane.showMessageDialog(null, menu, "Menu: M�sica",
				JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Retorna String com os dados de todas as m�sicas
	 * 
	 * @return Dados de todas as m�sicas
	 */
	private JScrollPane exibeTodos() {
		String textoTodos = musicas.toString();
		JTextArea texto = new JTextArea(20, 80);
		texto.setText(textoTodos);
		texto.setEditable(false);
		JScrollPane scroll = new JScrollPane(texto);
		return scroll;
	}

	private JScrollPane exibeAgrupado(String dados) {
		JTextArea texto = new JTextArea(20, 80);
		texto.setText(dados);
		texto.setEditable(false);
		JScrollPane scroll = new JScrollPane(texto);
		return scroll;
	}

	/**
	 * Grava os dados das m�sicas num arquivo de texto
	 * 
	 */
	public void gravar() {
		musicas.gravarArquivo();
	}
}
