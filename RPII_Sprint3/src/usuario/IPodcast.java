package usuario;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import midias.Midia;
import midias.Podcast;
import util.Data;
import dao.DAOPodcast;

/**
 * Classe para interação com o usuário.
 * 
 * @author Allan
 *
 */
public class IPodcast {

	private DAOPodcast podcasts;

	/**
	 * Construtor da interface.
	 */
	public IPodcast() {
		podcasts = new DAOPodcast();
	}

	/**
	 * Método de inclusão de um podcast.
	 */
	public void adiciona() {

		int op;

		boolean erro = false;

		String nomeDoArquivo = "";
		do{
			nomeDoArquivo = JOptionPane.showInputDialog(null,
				"Informe o nome do arquivo(destino): ");
		if(podcasts.existeArquivo(nomeDoArquivo)){
			JOptionPane.showMessageDialog(null, "O nome do arquivo(caminho) informado já existe, por favor informe um nome diferente(caminho).");
		}
		
		}while(podcasts.existeArquivo(nomeDoArquivo));	
		
		String titulo = JOptionPane.showInputDialog(null,
				"Informe o título do podcast: ");
		String descricao = JOptionPane.showInputDialog(null,
				"Qual a descrição do podcast?", "OPCIONAL");
		boolean error = false;
		String idioma = "";
		do{
		
		try{
		 idioma = JOptionPane.showInputDialog(null,
				"Informe o idioma do podcast: ");
			if(idioma.matches(".*\\d+.*")){
				throw new IllegalArgumentException("Informe apenas letras!");
			}
			error = false;
		}catch(IllegalArgumentException e){
				error = true;
				JOptionPane.showMessageDialog(null, e.getMessage(), "Podcasts - Erro", JOptionPane.ERROR_MESSAGE);
		}
		}while(error);
		
		ArrayList<String> autores = new ArrayList<String>();

		do {
			autores.add(JOptionPane.showInputDialog(null, "Autor: ", "PODCAST"));
			op = JOptionPane.showConfirmDialog(null,
					"Deseja adicionar mais um autor?", "Autores",
					JOptionPane.YES_NO_OPTION);

		} while (op == JOptionPane.YES_OPTION);

		Date data = null;
		do {
			try {
				data = Data.getAno(Integer.parseInt((JOptionPane
						.showInputDialog(null, "Informe o ano da criação: "))));
				erro = false;
			} catch (Exception e) {
				erro = true;
				JOptionPane.showMessageDialog(null, "Informe apenas números",
						"Podcast: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} while (erro);
		Midia podcast = new Podcast(nomeDoArquivo, titulo, descricao, idioma,
				autores, data);
		podcasts.add(podcast);

	}

	/**
	 * Método para deletar um podcast.
	 */
	public void exclui() {
		String[] titulos = podcasts.getTitulos();
		String titulo = (String) JOptionPane.showInputDialog(null,
				"Selecione o podcast que deseja excluir.", "Podcast",
				JOptionPane.PLAIN_MESSAGE, null, titulos, titulos[0]);
		if (titulo != null) {
			podcasts.exclui(titulo);
			JOptionPane.showMessageDialog(null,
					"O podcast foi excluido com sucesso.", "Podcast",
					JOptionPane.PLAIN_MESSAGE);
		}
	}

	/**
	 * Método para alteração de dados de uma midia do tipo podcast.
	 */
	public void altera() {

		String[] titulos = podcasts.getTitulos();
		String titulo = (String) JOptionPane.showInputDialog(null,
				"Selecione o podcast que deseja exibir.", "Podcast",
				JOptionPane.PLAIN_MESSAGE, null, titulos, titulos[0]);
		if (podcasts.existe(titulo)) {
			int alteraOutroCampo;
			Midia podcast = podcasts.get(titulo);
			String menu[] = { "Nome do arquivo", "Descrição", "Idioma",
					"Autores", "Ano" };
			do {
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Escolha o campo que deseja alterar",
						"Podcast: Alteração", JOptionPane.PLAIN_MESSAGE, null,
						menu, menu[0]);
				if (opcao.equalsIgnoreCase("Nome do arquivo")) {
					String nomeDoArquivo = JOptionPane.showInputDialog(null,
							"Nome do arquivo", "", JOptionPane.PLAIN_MESSAGE);

					((Podcast) podcast).setNomeDoArquivo(nomeDoArquivo);
					;
				} else if (opcao.equalsIgnoreCase("Descrição")) {
					String descricao = JOptionPane.showInputDialog(null,
							"Descrição", "Podcast", JOptionPane.PLAIN_MESSAGE);
					((Podcast) podcast).setDescricao(descricao);
				} else if (opcao.equalsIgnoreCase("Idioma")) {
					String idioma = JOptionPane.showInputDialog(null, "Idioma",
							"Podcast", JOptionPane.PLAIN_MESSAGE);
					((Podcast) podcast).setIdioma(idioma);
				} else if (opcao.equalsIgnoreCase("Autores")) {
					ArrayList<String> autores = new ArrayList<>();
					int autor;
					do {
						autores.add(JOptionPane.showInputDialog(null, "Autor",
								"Podcast", JOptionPane.PLAIN_MESSAGE));
						autor = JOptionPane.showConfirmDialog(null,
								"Deseja adicionar outro autor?", "Podcast",
								JOptionPane.YES_NO_OPTION);
					} while (autor == JOptionPane.YES_OPTION);
					((Podcast) podcast).setAutores(autores);
				} else if (opcao.equalsIgnoreCase("Ano")) {
					boolean erro = false;
					Date ano = null;
					do {
						try {
							ano = Data.getAno(Integer.parseInt(JOptionPane
									.showInputDialog(null, "Ano", "Podcast",
											JOptionPane.PLAIN_MESSAGE)));
							erro = false;
						} catch (Exception e) {
							erro = true;
							JOptionPane
									.showMessageDialog(
											null,
											"Informe apenas números inteiros positivos!",
											"Podcast: Erro",

											JOptionPane.ERROR_MESSAGE);
						}
					} while (erro);
					((Podcast) podcast).setAno(ano);
				}
				alteraOutroCampo = JOptionPane.showConfirmDialog(null,
						"Deseja alterar outro campo?", "Podcast: Alteração",
						JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
			} while (alteraOutroCampo == JOptionPane.YES_OPTION);
			podcasts.altera(titulo, podcast);
			JOptionPane.showMessageDialog(null,
					"O podcast foi alterado com sucesso.", "Podcast",
					JOptionPane.PLAIN_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null,
					"Não há podcasts cadastrados com esse titulo!",
					"Podcast: Erro", JOptionPane.ERROR_MESSAGE);
		}

	}

	/**
	 * Método que retorna todos os podcasts cadastrados com todos seus dados.
	 * 
	 * @return Todos os podcasts cadastrados e seus dados.
	 */
	public JScrollPane exibeTodos() {
		String dados = podcasts.toString();
		JTextArea txt = new JTextArea(20, 80);
		txt.setText(dados);
		txt.setEditable(false);
		JScrollPane scroll = new JScrollPane(txt);
		return scroll;
	}

	private void pesquisa() {
		String titulo = JOptionPane.showInputDialog(null,
				"Informe o título do podcast que deseja pesquisar.", "Podcast",
				JOptionPane.PLAIN_MESSAGE);
		if (podcasts.existe(titulo)) {
			JOptionPane.showMessageDialog(null,
					podcasts.get(titulo).toString(), "Podcast",
					JOptionPane.PLAIN_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null,
					"Não existe nenhum podcast cadastrado com esse título.",
					"Podcast: Erro", JOptionPane.ERROR_MESSAGE);
		}

		JButton agrupa = new JButton("Agrupa");
		agrupa.setBackground(new Color(210, 210, 210));
		agrupa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agrupa();

			}
		});
	}

	/**
	 * Menu com as opções de exibição.
	 */
	public void exibe() {
		String menu[] = { "Exibir um", "Exibir todos" };
		String opcao = (String) JOptionPane.showInputDialog(null, "Menu",
				"Podcast", JOptionPane.PLAIN_MESSAGE, null, menu, menu[0]);
		if (opcao != null) {
			if (opcao.equalsIgnoreCase("Exibir um")) {
				String[] titulos = podcasts.getTitulos();
				String titulo = (String) JOptionPane.showInputDialog(null,
						"Selecione o podcast que deseja exibir.", "Podcast",
						JOptionPane.PLAIN_MESSAGE, null, titulos, titulos[0]);
				if (titulo != null) {
					JOptionPane.showMessageDialog(null, podcasts.get(titulo),
							"Podcast", JOptionPane.PLAIN_MESSAGE);

				}
			} else if (opcao == null || opcao.equalsIgnoreCase("Exibir todos")) {
				JOptionPane.showMessageDialog(null, exibeTodos(), "PODCASTS",
						JOptionPane.PLAIN_MESSAGE);
			}
		}
	}

	/**
	 * Menu Podcast.
	 */
	public void menu() {
		JLabel lblPodcast = new JLabel("");
		lblPodcast.setHorizontalAlignment(SwingConstants.CENTER);
		lblPodcast.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/lblPodcast.png")));

		JButton adiciona = new JButton();
		adiciona.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastAdicionar.png")));
		adiciona.setBackground(new Color(210, 210, 210));
		adiciona.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adiciona();

			}
		});

		JButton altera = new JButton();
		altera.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastAlterar.png")));
		altera.setBackground(new Color(210, 210, 210));
		altera.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				altera();

			}
		});

		JButton exibe = new JButton();
		exibe.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastExibir.png")));
		exibe.setBackground(new Color(210, 210, 210));
		exibe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exibe();

			}
		});

		JButton agrupa = new JButton();
		agrupa.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastAgrupar.png")));
		agrupa.setBackground(new Color(210, 210, 210));
		agrupa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agrupa();

			}
		});

		JButton exclui = new JButton();
		exclui.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastExcluir.png")));
		exclui.setBackground(new Color(210, 210, 210));
		exclui.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exclui();

			}
		});

		Object[] menu = { lblPodcast, adiciona, altera, exibe, agrupa, exclui };
		JOptionPane.showMessageDialog(null, menu, "Menu: Podcast",
				JOptionPane.PLAIN_MESSAGE);
	}

	public void agrupa() {

		JButton idioma = new JButton();
		idioma.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastIdioma.png")));
		idioma.setBackground(new Color(210, 210, 210));
		idioma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String[] idiomas = podcasts.getIdiomas();
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Por qual idioma deseja agrupar?", "Podcasts",
						JOptionPane.PLAIN_MESSAGE, null, idiomas, idiomas[0]);
				if (opcao != null) {
					JOptionPane.showMessageDialog(null,
							exibeAgrupado(podcasts.agrupar("Idioma", opcao)),
							"Podcast - Idioma: " + opcao,
							JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

		JButton ano = new JButton();
		ano.setIcon(new ImageIcon(IPodcast.class
				.getResource("/usuario/podcastAno.png")));
		ano.setBackground(new Color(210, 210, 210));
		ano.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String[] anos = podcasts.getAnos();
				String opcao = (String) JOptionPane.showInputDialog(null,
						"Por qual ano deseja agrupar?", "Podcast",
						JOptionPane.PLAIN_MESSAGE, null, anos, anos[0]);
				if (opcao != null) {
					JOptionPane.showMessageDialog(null,
							exibeAgrupado(podcasts.agrupar("Ano", opcao)),
							"Podcast - Ano: " + opcao,
							JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

		Object[] menu = { idioma, ano };
		JOptionPane.showMessageDialog(null, menu, "Menu: Podcast",
				JOptionPane.PLAIN_MESSAGE);

	}

	private JScrollPane exibeAgrupado(String dados) {
		JTextArea texto = new JTextArea(20, 80);
		texto.setText(dados);
		texto.setEditable(false);
		JScrollPane scroll = new JScrollPane(texto);
		return scroll;
	}

	public void gravar() {
		podcasts.gravarArquivo();
	}

}
