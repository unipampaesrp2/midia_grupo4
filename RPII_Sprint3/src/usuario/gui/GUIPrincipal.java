package usuario.gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import usuario.IFoto;
import usuario.IMusica;
import usuario.IPodcast;

/**
 * GUI principal da aplica��o
 * 
 * @author Adriel Petry, Allan Pedroso, Eric Oliveira
 *
 */
@SuppressWarnings("serial")
public class GUIPrincipal extends JFrame {
	private IFoto foto;
	private IMusica musica;
	private IPodcast podcast;

	/**
	 * Executa a aplica��o
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIPrincipal frame = new GUIPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Cria o frame
	 */
	public GUIPrincipal() {
		setResizable(false);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();

//		foto = new IFoto();
		musica = new IMusica();
		podcast = new IPodcast();
		

		setTitle("M\u00EDdias");
		JOptionPane.setDefaultLocale(new Locale("pt", "BR"));
		setBounds(100, 100, 345, 335);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLocation((int) width / 2 - getWidth() / 2, (int) height / 2
				- getHeight() / 2);
		getContentPane().setLayout(null);

		JLabel bancoMidias = new JLabel("");
		bancoMidias.setHorizontalAlignment(SwingConstants.CENTER);
		bancoMidias.setIcon(new ImageIcon(GUIPrincipal.class
				.getResource("/usuario/banco_de_midias.png")));
		bancoMidias.setBounds(6, 11, 327, 34);
		getContentPane().add(bancoMidias);

		JButton btnFoto = new JButton("");
		btnFoto.setIcon(new ImageIcon(GUIPrincipal.class
				.getResource("/usuario/foto_bg.png")));
		btnFoto.setBounds(71, 56, 200, 66);
		btnFoto.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				foto.menu();
			}
		});
		getContentPane().add(btnFoto);

		JButton btnMusica = new JButton("");
		btnMusica.setIcon(new ImageIcon(GUIPrincipal.class
				.getResource("/usuario/musica_bg.png")));
		btnMusica.setBounds(71, 133, 200, 66);
		btnMusica.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				musica.menu();
			}
		});
		getContentPane().add(btnMusica);

		JButton btnPodcast = new JButton("");
		btnPodcast.setIcon(new ImageIcon(GUIPrincipal.class
				.getResource("/usuario/podcast_bg.png")));
		btnPodcast.setBounds(71, 210, 200, 66);
		btnPodcast.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				podcast.menu();
			}
		});
		getContentPane().add(btnPodcast);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				if (e.getID() == WindowEvent.WINDOW_CLOSING) {
					int opcao = JOptionPane.showConfirmDialog(getContentPane(),
							"Deseja realmente sair?", "Midias",
							JOptionPane.YES_NO_OPTION);
					if (opcao == JOptionPane.YES_OPTION) {
//						foto.gravar();
						musica.gravar();
						podcast.gravar();;
						System.exit(0);
					}
				}
			}
		});

	}
}
