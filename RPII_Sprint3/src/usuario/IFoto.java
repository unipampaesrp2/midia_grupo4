package usuario;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import util.Data;
import midias.Foto;
import midias.Midia;
import dao.DAOFoto;

/**
 * Classe IFoto
 * 
 * @author Matheus Aguiar, Eric Oliveira
 *
 */
public class IFoto {

	private DAOFoto fotos;

	/**
	 * Construtor da classe IFoto
	 * 
	 */
	public IFoto() {
		fotos = new DAOFoto();
	}

	/**
	 * Recebe dados de foto e a retorna como objeto Midia
	 * 
	 * @return Foto
	 */
	public Midia getFoto() {
		JOptionPane.showMessageDialog(null,
				"Escolha a foto na pr�xima janela.", "Foto",
				JOptionPane.PLAIN_MESSAGE);
		String caminho = "";
		String userHome = System.getProperty("user.home");
		JFileChooser nomeArquivo = new JFileChooser(userHome);
		int op = nomeArquivo.showOpenDialog(null);
		if (op == JFileChooser.APPROVE_OPTION) {
			File arquivo = nomeArquivo.getSelectedFile();
			caminho = arquivo.toString();
		}
		String nomeDoArquivo = caminho;
		String titulo = JOptionPane.showInputDialog(null, "T�tulo", "Foto",
				JOptionPane.PLAIN_MESSAGE);
		String descricao = JOptionPane.showInputDialog(null,
				"Descri��o\nDeixe em branco se n�o tiver uma", "Foto",
				JOptionPane.PLAIN_MESSAGE);
		String local = JOptionPane.showInputDialog(null, "Local", "Foto",
				JOptionPane.PLAIN_MESSAGE);
		String fotografo = JOptionPane.showInputDialog(null, "Fot�grafo",
				"Foto", JOptionPane.PLAIN_MESSAGE);
		ArrayList<String> pessoas = new ArrayList<>();
		int opcao;
		do {
			pessoas.add(JOptionPane.showInputDialog(null, "Pessoa", "Foto",
					JOptionPane.PLAIN_MESSAGE));
			opcao = JOptionPane.showConfirmDialog(null,
					"Deseja adicionar outra pessoa?", "Foto",
					JOptionPane.YES_NO_OPTION);
		} while (opcao == JOptionPane.YES_OPTION);
		Date data = Data.getData(JOptionPane.showInputDialog(null,
				"Data (dd/mm/aaaa)", "Foto", JOptionPane.PLAIN_MESSAGE));
		Date hora = Data.getHora(JOptionPane.showInputDialog(null,
				"Hora (hh:mm)", "Foto", JOptionPane.PLAIN_MESSAGE));
		Midia foto;
		try {
			foto = new Foto(nomeDoArquivo, titulo, descricao, fotografo,
					pessoas, local, data, hora);
		} catch (Exception e) {
			return null;
		}
		return foto;
	}

	/**
	 * Adiciona fotos ao banco de fotos
	 * 
	 */
	private void add() {
		int nova;
		do {
			Midia foto = getFoto();
			if (foto != null) {
				fotos.add(foto);
				JOptionPane.showMessageDialog(null,
						"Foto adicionada com sucesso!", "Foto",
						JOptionPane.PLAIN_MESSAGE);
			} else {
				JOptionPane
						.showMessageDialog(
								null,
								"N�o foi poss�vel adicionar a foto.\nDados insuficientes",
								"Foto: Erro", JOptionPane.ERROR_MESSAGE);
			}
			nova = JOptionPane.showConfirmDialog(null,
					"Deseja adicionar outra foto?", "Foto",
					JOptionPane.YES_NO_OPTION);
		} while (nova == JOptionPane.YES_OPTION);
	}

	/**
	 * Altera dados de uma foto
	 * 
	 */
	private void altera() {
		String titulo = JOptionPane.showInputDialog(null,
				"Informe o t�tulo da m�sica que deseja alterar.", "Foto",
				JOptionPane.PLAIN_MESSAGE);
		if (fotos.existe(titulo)) {
			Midia foto = getFoto();
			fotos.altera(titulo, foto);
			JOptionPane.showMessageDialog(null,
					"A foto foi alterada com sucesso.", "Foto",
					JOptionPane.PLAIN_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null,
					"N�o h� fotos cadastradas com esse t�tulo.", "Foto: Erro",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Exibe dados de uma ou de todas as fotos
	 * 
	 */
	private void exibe() {
		String menu[] = { "Uma foto", "Todas as fotos" };
		String opcao = (String) JOptionPane.showInputDialog(null, "Menu",
				"Foto", JOptionPane.PLAIN_MESSAGE, null, menu, menu[0]);
		if (opcao.equalsIgnoreCase("Uma foto")) {
			String titulo = JOptionPane.showInputDialog(null,
					"Informe o t�tulo da foto que deseja exibir.", "Foto",
					JOptionPane.PLAIN_MESSAGE);
			if (fotos.existe(titulo)) {
				JOptionPane.showMessageDialog(null, fotos.get(titulo), "Foto",
						JOptionPane.PLAIN_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null,
						"N�o h� fotos cadastradas com esse t�tulo.",
						"Foto: Erro", JOptionPane.ERROR_MESSAGE);
			}
		} else if (opcao == null || opcao.equalsIgnoreCase("Todas as fotos")) {
			JOptionPane.showMessageDialog(null, exibeTodos(), "Foto",
					JOptionPane.PLAIN_MESSAGE);
		}
	}

	/**
	 * Exclui uma foto do banco de fotos
	 * 
	 */
	private void exclui() {
		String titulo = JOptionPane.showInputDialog(null,
				"Informe o t�tulo da foto que deseja excluir.", "Foto",
				JOptionPane.PLAIN_MESSAGE);
		if (fotos.existe(titulo)) {
			fotos.exclui(titulo);
			JOptionPane.showMessageDialog(null,
					"A foto foi exclu�da com sucesso.", "Foto",
					JOptionPane.PLAIN_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null,
					"N�o h� fotos cadastradas com esse t�tulo.", "Foto: Erro",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Menu principal da classe
	 * 
	 */
	public void menu() {
		JLabel lblFoto = new JLabel("");
		lblFoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblFoto.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/lblFoto.png")));

		JButton adiciona = new JButton();
		adiciona.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/fotoAdicionar.png")));
		adiciona.setBackground(new Color(210, 210, 210));
		adiciona.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				add();

			}
		});

		JButton altera = new JButton();
		altera.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/fotoAlterar.png")));
		altera.setBackground(new Color(210, 210, 210));
		altera.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				altera();

			}
		});

		JButton exibe = new JButton();
		exibe.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/fotoExibir.png")));
		exibe.setBackground(new Color(210, 210, 210));
		exibe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exibe();

			}
		});

		JButton exclui = new JButton();
		exclui.setIcon(new ImageIcon(IMusica.class
				.getResource("/usuario/fotoExcluir.png")));
		exclui.setBackground(new Color(210, 210, 210));
		exclui.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				exclui();

			}
		});

		Object[] menu = { lblFoto, adiciona, altera, exibe, exclui };
		JOptionPane.showMessageDialog(null, menu, "Menu: Foto",
				JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Retorna String com os dados de todas as fotos
	 * 
	 * @return Dados de todas as fotos
	 */
	private JScrollPane exibeTodos() {
		String textoTodos = fotos.toString();
		JTextArea texto = new JTextArea(20, 80);
		texto.setText(textoTodos);
		texto.setEditable(false);
		JScrollPane scroll = new JScrollPane(texto);
		return scroll;
	}

	/**
	 * Grava os dados das fotos num arquivo de texto
	 * 
	 */
	public void gravar() {
		fotos.gravarArquivo();
	}

}
