package usuario;

import javax.swing.JOptionPane;

public class Principal {
	private static IMusica musica;
	private static IPodcast podcast;
	
	public static void main(String[] args) {
		musica = new IMusica();
		podcast = new IPodcast();
		String opcao;
		do {
			String menu[] = { "Foto", "M�sica", "Podcast", "Sair" };
			opcao = (String) JOptionPane.showInputDialog(null, "Menu",
					"M�dias", JOptionPane.PLAIN_MESSAGE, null, menu, menu[0]);
			if (opcao == null) {
				JOptionPane.showMessageDialog(null,
						"Para sair, escolha a op��o sair.", "M�dias",
						JOptionPane.INFORMATION_MESSAGE);
			}else if(opcao.equalsIgnoreCase("Foto")){
				
			}else if(opcao.equalsIgnoreCase("M�sica")){
				musica.menu();
			}else if(opcao.equalsIgnoreCase("Podcast")){
				podcast.menu();
			}
		} while (opcao == null || !opcao.equalsIgnoreCase("Sair"));
		musica.gravar();
	}

}
