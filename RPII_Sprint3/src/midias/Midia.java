package midias;

import java.io.Serializable;

/**
 * Classe Midia
 * 
 * @author Adriel Petry, Allan Pedroso, Eric Oliveira
 *
 */
public class Midia implements Serializable{

	protected String nomeDoArquivo;

	protected String titulo;

	protected String descricao;

	/**
	 * Construtor da classe Midia
	 * 
	 * @param nomeDoArquivo
	 *            Nome (caminho absoluto) da m�dia
	 * @param titulo
	 *            T�tulo da m�dia
	 * @param descricao
	 *            Descri��o da m�dia
	 */
	public Midia(String nomeDoArquivo, String titulo, String descricao) {
		this.nomeDoArquivo = nomeDoArquivo;
		this.titulo = titulo;
		this.descricao = descricao;
	}

	/**
	 * Retorna o nome (caminho absoluto) da m�dia
	 * 
	 * @return Nome (caminho absoluto) da m�dia
	 */
	public String getNomeDoArquivo() {
		return nomeDoArquivo;
	}

	/**
	 * Atribui o nome (caminho absoluto) � m�dia
	 * 
	 * @param nomeDoArquivo
	 *            Nome (caminho absoluto) da m�dia
	 */
	public void setNomeDoArquivo(String nomeDoArquivo) {
		this.nomeDoArquivo = nomeDoArquivo;
	}

	/**
	 * Retorna o t�tulo da m�dia
	 * 
	 * @return T�tulo da m�dia
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * Atribui o t�tulo � m�dia
	 * 
	 * @param titulo
	 *            T�tulo da m�dia
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * Retorna a descri��o da m�dia
	 * 
	 * @return Descri��o da m�dia
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Atribui a descri��o � m�dia
	 * 
	 * @param descricao
	 *            Descri��o da m�dia
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
