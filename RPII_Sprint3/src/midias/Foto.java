package midias;

import java.util.ArrayList;
import java.util.Date;

import util.Data;

/**
 * Classe Foto que herda atributos e m�todos da classe Midia
 * 
 * @author Matheus Aguiar, Eric Oliveira
 *
 */
public class Foto extends Midia {

	private String fotografo;

	private ArrayList<String> pessoas;

	private String local;

	private Date data;

	private Date hora;

	/**
	 * Construtor da classe Foto
	 * 
	 * @param nomeDoArquivo
	 *            Nome (caminho absoluto) da foto
	 * @param titulo
	 *            T�tulo da foto
	 * @param descricao
	 *            Descri��o da foto
	 * @param fotografo
	 *            Fot�grafo
	 * @param pessoas
	 *            Pessoas que aparecem na foto
	 * @param local
	 *            Local da foto
	 * @param data
	 *            Data na qual a foto foi tirada
	 * @param hora
	 *            Hora na qual a foto foi tirada
	 */
	public Foto(String nomeDoArquivo, String titulo, String descricao,
			String fotografo, ArrayList<String> pessoas, String local,
			Date data, Date hora) {
		super(nomeDoArquivo, titulo, descricao);
		this.fotografo = fotografo;
		this.pessoas = pessoas;
		this.local = local;
		this.data = data;
		this.hora = hora;
	}

	/**
	 * Retorna o fot�grafo
	 * 
	 * @return Fot�grafo
	 */
	public String getFotografo() {
		return fotografo;
	}

	/**
	 * Atribui o fot�grafo � foto
	 * 
	 * @param fotografo
	 *            Fot�grafo
	 */
	public void setFotografo(String fotografo) {
		this.fotografo = fotografo;
	}

	/**
	 * Retorna a(s) pessoa(s) que aparece(m) na foto
	 * 
	 * @return Pessoa(s) que aparece(m) na foto
	 */
	public ArrayList<String> getPessoas() {
		return pessoas;
	}

	/**
	 * Atribui a(s) pessoa(s) que aparece(m) na foto
	 * 
	 * @param pessoas
	 *            Pessoa(s) que aparece(m) na foto
	 */
	public void setPessoas(ArrayList<String> pessoas) {
		this.pessoas = pessoas;
	}

	/**
	 * Retorna o local da foto
	 * 
	 * @return Local da foto
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * Atribu local � foto
	 * 
	 * @param local
	 *            Local da foto
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * Retorna a data na qual a foto foi tirada
	 * 
	 * @return Data na qual a foto foi tirada
	 */
	public Date getData() {
		return data;
	}

	/**
	 * Atribui data � foto
	 * 
	 * @param data
	 *            Data na qual a foto foi tirada
	 */
	public void setData(Date data) {
		this.data = data;
	}

	/**
	 * Retorna a hora na qual a foto foi tirada
	 * 
	 * @return Hora na qual a foto foi tirada
	 */
	public Date getHora() {
		return hora;
	}

	/**
	 * Atribui hora � foto
	 * 
	 * @param hora
	 *            Hora na qual a foto foi tirada
	 */
	public void setHora(Date hora) {
		this.hora = hora;
	}

	/**
	 * Retorna uma String com os dados da foto
	 * 
	 * @return Dados da foto
	 */
	@Override
	public String toString() {
		String str = "Nome do arquivo: " + nomeDoArquivo + "\n";
		str += "T�tulo: " + titulo + "\n";
		str += "Descri��o: " + descricao + "\n";
		str += "Fot�grafo: " + fotografo + "\n";
		str += "Pessoa(s): ";
		for (int i = 0; i < pessoas.size(); i++) {
			str += pessoas.get(i);
			if (i < pessoas.size() - 2) {
				str += ", ";
			} else if (i < pessoas.size() - 1) {
				str += " e ";
			}
		}
		str += "\n";
		str += "Local: " + local + "\n";
		str += "Data: " + Data.dataToString(data) + "\n";
		str += "Hora: " + Data.horaToString(hora) + "\n";
		return str;
	}

}
