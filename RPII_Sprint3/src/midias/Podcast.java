package midias;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import util.Data;

/**
 * Classe com os atributos do objeto podcast, construtor do mesmo e métodos toString.
 * @author Allan Pedroso.
 *
 */
public class Podcast extends Sonora implements Serializable {

	/**
	 * Construtor da midia podcast.
	 * @param nomeDoArquivo Nome do arquivo(destino).
	 * @param titulo Titulo do podcast.
	 * @param descricao Descrição.
	 * @param idioma idioma.
	 * @param autores Autores.
	 * @param ano Ano.
	 */
	public Podcast(String nomeDoArquivo, String titulo, String descricao,
			String idioma, ArrayList<String> autores, Date ano) {
		super(nomeDoArquivo, titulo, descricao, idioma, autores, ano);

	}

	/**
	 * Método que retorna todas os valores do arrayList de autores em uma string
	 * @return
	 */
	public String autorestoString() {
		String dadoAutores = " ";
		for (int i = 0; i < autores.size(); i++) {
			dadoAutores += autores.get(i);
			if (i < autores.size() - 2) {
				dadoAutores += ", ";
			} else if (i < autores.size() - 1) {
				dadoAutores += " e ";
			}
		}
		return dadoAutores + ".";
	}

	/**
	 * Método que retorna uma string com todos os dados de um podcast.
	 */
	public String toString() {

		String dadosMidia = "";

		dadosMidia += "Nome: " + nomeDoArquivo + "\nTítulo: " + titulo
				+ "\nDescrição :" + descricao + "\nIdioma: " + idioma
				+ "\nAutor(es): " + autorestoString() + "\nAno: "
				+ Data.anoToString(ano) + "\n";

		return dadosMidia;
	}

}