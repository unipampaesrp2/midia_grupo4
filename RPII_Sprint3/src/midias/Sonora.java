package midias;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Classe que extende a classe midia, contendo seus atributos pr�prios e m�todos getters e setters.
 * @author Adriel Petry, Allan Pedroso, Eric Oliveira.	
 *
 */
public class Sonora extends Midia implements Serializable{
 
	protected String idioma;
	 
	protected ArrayList<String> autores;
	 
	protected Date ano;
	 
	/**
	 * Construtor.
	 * @param nomeDoArquivo nome do arquivo(destino).
	 * @param titulo titulo da midia.
	 * @param descricao descri��o.
	 * @param idioma idioma.
	 * @param autores autores.
	 * @param ano ano.
	 */
	public Sonora(String nomeDoArquivo, String titulo, String descricao,
			String idioma, ArrayList<String> autores, Date ano) {
		super(nomeDoArquivo, titulo, descricao);
		this.idioma = idioma;
		this.autores = autores;
		this.ano = ano;
	}

	/**
	 * M�todo que retorna o idioma.
	 * @return idioma.
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * M�todo que altera o idioma.
	 * @param idioma Idioma.
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * M�todo que retorna os autores.
	 * @return autores.
	 */
	public ArrayList<String> getAutores() {
		return autores;
	}

	/**
	 * M�todo que altera autores.
	 * @param autores autores.
	 */
	public void setAutores(ArrayList<String> autores) {
		this.autores = autores;
	}

	/**
	 * M�todo que retorna o ano.
	 * @return ano.
	 */
	public Date getAno() {
		return ano;
	}

	/**
	 * M�todo que altera o ano.
	 * @param ano ano.
	 */
	public void setAno(Date ano) {
		this.ano = ano;
	}

		 
}
 
