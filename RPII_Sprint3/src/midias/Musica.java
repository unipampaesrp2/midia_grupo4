package midias;

import java.io.Serializable;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import util.Data;

/**
 * Classe Musica que herda atributos e m�todos da classe Sonora
 * 
 * @author Eric Oliveira
 *
 */
public class Musica extends Sonora implements Serializable, Comparable<Musica>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5463883924992784822L;

	private String genero;

	private ArrayList<String> interpretes;

	private int duracao;

	/**
	 * Cosntrutor da classe Musica
	 * 
	 * @param nomeDoArquivo
	 *            Nome (caminho absoluto) da m�sica
	 * @param titulo
	 *            T�tulo da m�sica
	 * @param descricao
	 *            Descri��o da m�sica
	 * @param genero
	 *            G�nero da m�sica
	 * @param idioma
	 *            Idioma da m�sica
	 * @param autores
	 *            Autor(es) da m�sica
	 * @param ano
	 *            Ano da m�sica
	 * @param interpretes
	 *            Int�rprete(s) da m�sica
	 * @param duracao
	 *            Dura��o da m�sica
	 */
	public Musica(String nomeDoArquivo, String titulo, String descricao,
			String genero, String idioma, ArrayList<String> autores, Date ano,
			ArrayList<String> interpretes, int duracao) {
		super(nomeDoArquivo, titulo, descricao, idioma, autores, ano);
		this.genero = genero;
		this.interpretes = interpretes;
		this.duracao = duracao;
	}
	
	/**
	 * Retorna o g�nero da m�sica
	 * 
	 * @return G�nero da m�sica
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * Atribui o g�nero � m�sica
	 * 
	 * @param genero
	 *            G�nero da m�sica
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * Retorna o(s) int�rprete(s) da m�sica
	 * 
	 * @return Int�rprete(s) da m�sica
	 */
	public ArrayList<String> getInterpretes() {
		return interpretes;
	}

	/**
	 * Atribui int�rprete(s) � m�sica
	 * 
	 * @param interpretes
	 *            Intr�prete(s) da m�sica
	 */
	public void setInterpretes(ArrayList<String> interpretes) {
		this.interpretes = interpretes;
	}

	/**
	 * Atribui int�rprete � m�sica
	 * 
	 * @param interprete
	 *            Int�rprete da m�sica
	 */
	public void setInterprete(String interprete) {
		this.interpretes.add(interprete);
	}

	/**
	 * Retorna a dura��o da m�sica
	 * 
	 * @return Dura��o da m�sica
	 */
	public int getDuracao() {
		return duracao;
	}

	/**
	 * Atribui dura��o � m�sica
	 * 
	 * @param duracao
	 *            Dura��o da m�sica
	 */
	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	/**
	 * Retorna uma String com os dados da m�sica
	 * 
	 * @return Dados da m�sica
	 */
	@Override
	public String toString() {
		String str = "Nome do arquivo: " + nomeDoArquivo + "\n";
		str += "T�tulo: " + titulo + "\n";
		str += "Descri��o: " + descricao + "\n";
		str += "G�nero: " + genero + "\n";
		str += "Idioma: " + idioma + "\n";
		str += "Autor(es): ";
		for (int i = 0; i < autores.size(); i++) {
			str += autores.get(i);
			if (i < autores.size() - 2) {
				str += ", ";
			} else if (i < autores.size() - 1) {
				str += " e ";
			}
		}
		str += "\n";
		str += "Ano: " + Data.anoToString(ano) + "\n";
		str += "Int�rprete(s): ";
		for (int i = 0; i < interpretes.size(); i++) {
			str += interpretes.get(i);
			if (i < interpretes.size() - 2) {
				str += ", ";
			} else if (i < interpretes.size() - 1) {
				str += " e ";
			}
		}
		str += "\n";
		int minutos = duracao / 60;
		int segundos = duracao % 60;
		str += "Dura��o: " + minutos + ":" + segundos + "\n";
		return str;
	}

	@Override
	public int compareTo(Musica m) {
		Collator compara = Collator.getInstance(new Locale("pt", "BR"));
		compara.setStrength(Collator.PRIMARY);
		if(compara.compare(titulo, m.getTitulo()) < 0){
			return -1;
		}else if(compara.compare(titulo, m.getTitulo()) == 0){
			return 0;
		}else{
			return 1;
		}
	}
}
